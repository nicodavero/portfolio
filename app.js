const lettersA = document.querySelector('#lettersA');
const lettersB = document.querySelector('#lettersB');
const lettersC = document.querySelector('#lettersC');

const tl = new TimelineMax();

tl.fromTo(lettersA, 1.5, {y: 300, opacity: 0.4}, {y: 0, opacity:1, ease: Power1.easeInOut})
.fromTo(lettersB, 1.5, {y: 300, opacity: 0.4}, {y: 0, opacity:1, ease: Power1.easeInOut})
.fromTo(lettersC, 1.5, {y: 300, opacity: 0.4}, {y: 0, opacity:1, ease: Power1.easeInOut}, "-=0.6")

gsap.utils.toArray('.text-from-left').forEach((el, index) => { 
    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: el,
        start: "center bottom",
        toggleActions: "play none none none",
        ease: "power1.inOut",
        once: true
      }
    })
    
    tl
    .fromTo(el, {opacity: 0.4, x: -800}, {opacity: 1, x: 0, duration: 1.5, ease: Power1.easeInOut, immediateRender: false})
});

gsap.utils.toArray('.bg-quote img').forEach((el, index) => { 
    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: el,
        start: "center bottom",
        toggleActions: "play none none none",
        ease: "power1.inOut",
        once: true
      }
    })  

    .to({}, {duration: 0.5, ease: Power4.easeInOut, immediateRender: false})

    tl
    .fromTo(el, {opacity: 0}, {opacity: 1, duration: 2, ease: Power1.easeInOut, immediateRender: false})
});

gsap.utils.toArray('.text-from-bottomA').forEach((el, index) => { 
    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: el,
        start: "top bottom",
        toggleActions: "play none none none",
        ease: "power1.inOut",
        once: true
      }
    })
    
    .to({}, {duration: 1, ease: Power1.easeInOut, immediateRender: false})

    tl
    .fromTo(el, {opacity: 0.4, y: 200}, {opacity: 1, y: 0, duration: 2, ease: Power1.easeInOut, immediateRender: false})
});

gsap.utils.toArray('.text-from-bottomB').forEach((el, index) => { 
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: el,
      start: "top bottom",
      toggleActions: "play none none none",
      ease: "power1.inOut",
      once: true
    }
  })
  
  .to({}, {duration: 2.5, ease: Power1.easeInOut, immediateRender: false})

  tl
  .fromTo(el, {opacity: 0.4, y: 200}, {opacity: 1, y: 0, duration: 2, ease: Power1.easeInOut, immediateRender: false})
});

gsap.utils.toArray('.from-right').forEach((el, index) => { 
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: el,
      start: "center bottom",
      toggleActions: "play none none none",
      ease: "power1.inOut",
      once: true
    }
  })
  
  .to({}, {duration: 1.2, ease: Power1.easeInOut, immediateRender: false})

  tl
  .fromTo(el, {opacity: 0.4, x: 1000}, {opacity: 1, x: 0, duration: 1.5, ease: Power1.easeInOut, immediateRender: false})
});

gsap.utils.toArray('.from-right-text').forEach((el, index) => { 
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: el,
      start: "top bottom",
      toggleActions: "play none none none",
      ease: "power1.inOut",
      once: true
    }
  })
  
  tl
  .fromTo(el, {opacity: 0.4, x: 1000}, {opacity: 1, x: 0, duration: 1.5, ease: Power1.easeInOut, immediateRender: false})
});

const sliderContainer = document.querySelector(".slider-container");
const slideRight = document.querySelector(".right-slide");
const slideLeft = document.querySelector(".left-slide");
const upButton = document.querySelector(".up-button");
const downButton = document.querySelector(".down-button");
const slidesLength = slideRight.querySelectorAll("div").length;

let activeSlideIndex = 0;

slideLeft.style.top = `-${(slidesLength - 1) * 100}vh`;

const changeSlide = (direction) => {
  const sliderHeight = sliderContainer.clientHeight;
  if (direction === "up") {
    activeSlideIndex++;
    if (activeSlideIndex > slidesLength - 1) activeSlideIndex = 0;
  } else if (direction === "down") {
    activeSlideIndex--;
    if (activeSlideIndex < 0) activeSlideIndex = slidesLength - 1;
  }
  slideRight.style.transform = `translateY(-${
    activeSlideIndex * sliderHeight
  }px)`;
  slideLeft.style.transform = `translateY(${
    activeSlideIndex * sliderHeight
  }px)`;
};

upButton.addEventListener("click", () => changeSlide("up"));
downButton.addEventListener("click", () => changeSlide("down"));


